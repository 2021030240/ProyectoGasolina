/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

/**
 *
 * @author Administrator
 */
public class Bomba {
    private int numBomba;
    private float capacidad;
    private float contadorL;
    private Gasolina gasolina;

    public Bomba() {
        this.numBomba = 0;
        this.capacidad = 0.0f;
        this.contadorL = 0.0f;
        this.gasolina = new Gasolina();
        
    }
    public Bomba(int numBomba, float capacidad, float contadorL, Gasolina gasolina) {
        this.numBomba = numBomba;
        this.capacidad = capacidad;
        this.contadorL = contadorL;
        this.gasolina = gasolina;
    }
    public Bomba(Bomba bomba) {
        this.numBomba = bomba.numBomba;
        this.capacidad = bomba.capacidad;
        this.contadorL = bomba.contadorL;
        this.gasolina = bomba.gasolina;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public float getContadorL() {
        return contadorL;
    }

    public void setContadorL(float contadorL) {
        this.contadorL = contadorL;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    public float calcularInventario()
    {
        return this.capacidad-this.contadorL;
    }
    public boolean realizarVenta(float cantidad)
    {
        boolean exito=false;
        if(cantidad<=this.calcularInventario())
            exito = true;
        return exito;
    }
    public float calcularImporte(float precio){
        //return this.contadorL * this.gasolina.getPrecio();
        return this.contadorL * precio; //De la menera original no me dejo profe me daba el resultado = 0, revise las variables y si se estan guardando bien.
    }
    
    
}
