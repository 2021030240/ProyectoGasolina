/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
/**
 *
 * @author _Windows_
 */
public class jintGasolina extends javax.swing.JInternalFrame {

    /**
     * Creates new form JintCotizacion
     */
    public jintGasolina() {
        initComponents();
        this.desahabilitar();
        this.resize(1200,900);
        getContentPane().setBackground(new Color(34, 34, 34));
    }
    public void desahabilitar(){
        this.btnVender.setEnabled(false);
        this.txtCantidad.setEnabled(false);
        this.txtContador.setEnabled(false);
    }

    
    public void habilitar(){
         this.btnVender.setEnabled(true);
        this.txtCantidad.setEnabled(true);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        btnIniciar = new javax.swing.JButton();
        cmbTipo = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtContador = new javax.swing.JTextField();
        jslCapacidad = new javax.swing.JSlider();
        pamVenta = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        lblCostoVenta = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        btnVender = new javax.swing.JButton();
        lblMensaje = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        txtBomba = new javax.swing.JTextField();

        setBackground(new java.awt.Color(41, 41, 39));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        getContentPane().setLayout(null);

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("PEMEX");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(500, 40, 300, 90);

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Precio");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(710, 220, 100, 90);

        txtPrecio.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtPrecio.setText("0");
        txtPrecio.setToolTipText("");
        getContentPane().add(txtPrecio);
        txtPrecio.setBounds(820, 250, 110, 40);

        btnIniciar.setFont(new java.awt.Font("Tw Cen MT", 0, 24)); // NOI18N
        btnIniciar.setText("INICIAR BOMBA");
        btnIniciar.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        btnIniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarActionPerformed(evt);
            }
        });
        getContentPane().add(btnIniciar);
        btnIniciar.setBounds(950, 180, 200, 110);

        cmbTipo.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        cmbTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tipo de Gasolina", "Premium", "Regular" }));
        getContentPane().add(cmbTipo);
        cmbTipo.setBounds(210, 230, 210, 40);

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Bomba ");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(30, 150, 130, 90);

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Tipo de Gasolina");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(190, 150, 270, 90);

        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Contador de Litros");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(530, 150, 270, 90);

        txtContador.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtContador.setText("0");
        txtContador.setToolTipText("");
        getContentPane().add(txtContador);
        txtContador.setBounds(820, 180, 110, 40);

        jslCapacidad.setBackground(new java.awt.Color(51, 51, 51));
        jslCapacidad.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jslCapacidad.setForeground(new java.awt.Color(255, 255, 255));
        jslCapacidad.setMajorTickSpacing(10);
        jslCapacidad.setPaintLabels(true);
        jslCapacidad.setPaintTicks(true);
        jslCapacidad.setMaximumSize(new java.awt.Dimension(32767, 100));
        jslCapacidad.setMinimumSize(new java.awt.Dimension(100, 100));
        jslCapacidad.setPreferredSize(new java.awt.Dimension(10000, 40));
        jslCapacidad.setValueIsAdjusting(true);
        jslCapacidad.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jslCapacidadStateChanged(evt);
            }
        });
        jslCapacidad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jslCapacidadMouseClicked(evt);
            }
        });
        getContentPane().add(jslCapacidad);
        jslCapacidad.setBounds(30, 300, 1120, 90);

        pamVenta.setBackground(new java.awt.Color(51, 51, 51));
        pamVenta.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Venta de Gasolina", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 0, 36), new java.awt.Color(255, 255, 255))); // NOI18N
        pamVenta.setFont(new java.awt.Font("Trebuchet MS", 0, 18)); // NOI18N
        pamVenta.setLayout(null);

        lblCostoVenta.setBackground(new java.awt.Color(255, 255, 255));
        lblCostoVenta.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        lblCostoVenta.setText("Cantidad:");

        txtCantidad.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtCantidad.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCantidad.setText("0");
        txtCantidad.setToolTipText("");
        txtCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCantidadActionPerformed(evt);
            }
        });

        btnVender.setFont(new java.awt.Font("Tw Cen MT", 0, 36)); // NOI18N
        btnVender.setText("Vender");
        btnVender.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        btnVender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenderActionPerformed(evt);
            }
        });

        lblMensaje.setBackground(new java.awt.Color(255, 255, 255));
        lblMensaje.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        lblMensaje.setText("Mensaje:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(115, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblCostoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(btnVender)))
                .addGap(96, 96, 96))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCostoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnVender, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(lblMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(47, Short.MAX_VALUE))
        );

        pamVenta.add(jPanel1);
        jPanel1.setBounds(80, 120, 750, 210);

        lblTotal.setBackground(new java.awt.Color(255, 255, 255));
        lblTotal.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(255, 255, 255));
        lblTotal.setText("TOTAL VENTA:   $");
        pamVenta.add(lblTotal);
        lblTotal.setBounds(450, 330, 370, 60);

        getContentPane().add(pamVenta);
        pamVenta.setBounds(150, 430, 940, 460);

        txtBomba.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtBomba.setText("0");
        txtBomba.setToolTipText("");
        getContentPane().add(txtBomba);
        txtBomba.setBounds(30, 230, 110, 40);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarActionPerformed
        bom = new Bomba();
        gas = new Gasolina();
        boolean exito = false;
        int tipo=0;
        if(this.txtCantidad.getText().equals(""))exito = true;
        if(this.txtBomba.getText().equals(""))exito = true;
        if(this.txtPrecio.getText().equals(""))exito = true;
        if(this.cmbTipo.getSelectedIndex()==0)exito=true;//Verificar que no este por defecto el cmb
        
        if(exito==true){
            //Falto informacion
            JOptionPane.showMessageDialog(this,"Falto Capturar Información");
        }
        else{
            this.habilitar();
            //Todo esta correcto
            bom.setCapacidad(this.jslCapacidad.getValue());
            
            gas.setPrecio(Float.parseFloat(this.txtPrecio.getText()));
            bom.setNumBomba(Integer.parseInt(this.txtBomba.getText()));
            bom.setContadorL(Float.parseFloat(this.txtContador.getText()));
            tipo = this.cmbTipo.getSelectedIndex();
            gas.setTipo(this.cmbTipo.getSelectedIndex());
            switch (tipo){
                case 1 :gas.setTipo(1);break;
                case 2 :gas.setTipo(2);break;
            }
            JOptionPane.showMessageDialog(this,"Se Guardo Correctamente la Información");
        
        }
        
    }//GEN-LAST:event_btnIniciarActionPerformed

    private void btnVenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenderActionPerformed
       if(bom.realizarVenta(Float.parseFloat(this.txtCantidad.getText()))==true)
       {
            float cantidad = Float.parseFloat(this.txtCantidad.getText());
            float preciof=0.0f,cantidadF;
            bom.setContadorL(bom.getContadorL()+cantidad);
            this.txtContador.setText(String.valueOf(bom.getContadorL()));
            preciof=bom.calcularImporte(gas.getPrecio());
            this.lblTotal.setText("Total de Venta: " + preciof + "$");
            //Hacer que la barra Baje
            cantidadF = bom.getCapacidad()- bom.getContadorL();
            int cantidadInt = Math.round(cantidadF);//Convertir a INT de Float
            this.jslCapacidad.setValue(cantidadInt);
           JOptionPane.showMessageDialog(this,"Se Realizo la Venta Correctamente");
       }
       else
           JOptionPane.showMessageDialog(this,"No hay suficiente litros para realizar la venta");
// TODO add your handling code here:
    }//GEN-LAST:event_btnVenderActionPerformed

    private void txtCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCantidadActionPerformed

    private void jslCapacidadMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jslCapacidadMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jslCapacidadMouseClicked

    private void jslCapacidadStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jslCapacidadStateChanged
        // TODO add your handling code here:

    }//GEN-LAST:event_jslCapacidadStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIniciar;
    private javax.swing.JButton btnVender;
    private javax.swing.JComboBox<String> cmbTipo;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSlider jslCapacidad;
    private javax.swing.JLabel lblCostoVenta;
    private javax.swing.JLabel lblMensaje;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JPanel pamVenta;
    private javax.swing.JTextField txtBomba;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtContador;
    private javax.swing.JTextField txtPrecio;
    // End of variables declaration//GEN-END:variables

    
    
    private Bomba bom;
    private Gasolina gas;
    
    
    
}